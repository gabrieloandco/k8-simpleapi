import os
import pytest
from falcon import testing
from apps.users.models import Base
from sqlalchemy import create_engine
from app import app as application

#TEST_URI='postgresql+psycopg2://postgres:postgres@localhost/test_db'
TEST_URI='postgresql+psycopg2://%s:%s@%s/%s' % (os.environ.get('DB_USER'),os.environ.get('DB_PASS'), os.environ.get('DB_HOST'),os.environ.get('DB_NAME'))
engine = create_engine(TEST_URI)

@pytest.fixture(scope='session')
def client(request):
    Base.metadata.create_all(engine)

    def teardown():
        Base.metadata.drop_all(bind=engine)

    request.addfinalizer(teardown)

    return testing.TestClient(application)

def test_post_user(client):
    endpoint = '/users'
    resp= client.simulate_post(endpoint, json={'email': 'test1@test1.com', 'password': '1234'})
    assert resp.status_code==200

def test_get_users(client):
    endpoint = '/users'
    resp = client.simulate_get(endpoint)
    assert resp.status_code == 200

def test_get_user(client):
    post_endpoint = '/users'
    client.simulate_post(post_endpoint, json={'email': 'test2@test2.com', 'password': '1234'})
    client.simulate_get(post_endpoint)
    endpoint = '/users/2'
    resp = client.simulate_get(endpoint)
    assert resp.status_code == 200


def test_delete_user(client):
    post_endpoint = '/users'
    client.simulate_post(post_endpoint, json={'email': 'test@test.com', 'password': '1234'})
    endpoint = '/users/1'
    resp = client.simulate_delete(endpoint)
    assert resp.status_code == 200

