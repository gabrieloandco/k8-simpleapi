import falcon
import psycopg2

from falcon_autocrud.middleware import Middleware
from apps.users.urls import *
import falcon_jsonify 
from settings.environment import*

from apps.users.models import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from os import environ
from settings.environment import DB_URI

# Create engine
engine = create_engine(DB_URI)

# Create All Tables
Base.metadata.create_all(engine)

#import your urls here
app = application = falcon.API(
    middleware=[Middleware()],
    
)




app.add_route(user_app_urls['user_collection'][0],user_app_urls['user_collection'][1])
app.add_route(user_app_urls['user_single'][0],user_app_urls['user_single'][1])
app.add_route(user_app_urls['user_signin'][0],user_app_urls['user_signin'][1])

 
 

