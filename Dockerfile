FROM python:3.5
ARG ROOT_DIR
# Required to install all requirements into the ticking dir.

ENV PYTHONUNBUFFERED 1
#ENV PATH $PATH:${PYTHONUSERBASE}:${PYTHONUSERBASE}/bin/:${PYTHONUSERBASE}/lib/python3.5/site-packages/
#ENV PATH $PATH:/home/ticking_user/.local/bin
# home/ticking_user/.local/lib/python3.5/site-packages/unipath/abstractpath.py

# Create non-privileged user
RUN groupadd -g 999 ticking_g && \
    useradd -r -u 999 -g ticking_g ticking_user

# Copy app code
WORKDIR /rest-api/
ADD . /rest-api/

# Requirements.
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir json-logging-py


RUN rm requirements.txt
RUN rm -r Dockerfile .gitignore .git README.md
ENTRYPOINT gunicorn -b 0.0.0.0:8000 app