import os

def gen_models(app_path, app_name):
    env_path = 'settings.environment'
    models_file = open(app_path+'/models.py',"a+")
    content = [
            'from sqlalchemy import *\n',
            'from sqlalchemy.ext.declarative import declarative_base \n',
            'from ' + env_path + ' import DB_URI\n',
            'from apps.utils import Base\n',
            'APP_NAME = \'' + app_name + '\'\n\n',
            'Base = declarative_base(cls=Base)\n',
            'engine = create_engine(DB_URI)\n',
            'metadata = MetaData(bind=engine)\n\n',
            '# Write below your models, heres a functional example:\n\n',
            'class ' + app_name.capitalize() + '(Base):\n',
            '\t__table__ = Table(\'' + app_name +'s_' + app_name + '\', metadata, autoload=True)\n\n',
            ]
    for line in content:
        models_file.write(line)
    models_file.close()


def gen_resources(app_path, app_name):
    resources_file = open(app_path+'/resources.py',"a+")
    content = [
            'from falcon_autocrud.resource import CollectionResource, SingleResource\n',
            'from .models import * \n\n',
            '# Write below the resources of your endpoint, heres a functional CRUD example:\n',
            'class ' + app_name.capitalize() + 'CollectionResource(CollectionResource):\n',
            '# this are his defaults methods, limit them as you wish.\n',
            '\tmethods = [\'GET\', \'POST\']\n',
            '\tmodel = ' + app_name.capitalize() + '\n\n',
            'class ' + app_name.capitalize() + 'SingleResource(SingleResource):\n',
            '# this are his defaults methods, limit them as you wish.\n',
            '\tmethods = [\'GET\',\'PUT\',\'PATCH\',\'DELETE\']\n',
            '\tmodel = ' + app_name.capitalize() + '\n'
            ]
    for line in content:
        resources_file.write(line)
    resources_file.close()


def gen_urls(app_path, app_name):
    env_path = 'settings.environment'
    urls_file = open(app_path + '/urls.py',"a+")
    content = [
        'from ' + env_path + ' import DB_URI\n',
        'from sqlalchemy import create_engine\n',
        'from .resources import *\n\n',
        'db_engine = create_engine(DB_URI)\n\n',
        '# Write below your urls with his resources into a tuple.\n',
        app_name + '_app_urls = {\n\n',
        '\'' + app_name + '_collection\' : (\'/' + app_name+'s' + '\', ' + app_name.capitalize() + 'CollectionResource(db_engine)),\n',
        '\'' + app_name + '_single\' : (\'/' + app_name +'s'+ '/{pk}\', ' + app_name.capitalize() + 'SingleResource(db_engine))\n\n',
        '}'
    ]
    for line in content:
        urls_file.write(line)
    urls_file.close()


def edit_app (_path, app_name):
    app_file = open(_path+'/app.py', "r+")
    old_lines = app_file.readlines()
    app_file.seek(0)
    app_file.write('from apps.' + app_name+'s' + '.urls import *\n')
    for line in old_lines:
        app_file.write(line)
    app_file.close()
    app_file = open(_path + '/app.py', "a+")
    app_file.write('\n# Write below the ' + app_name + ' app routes.\n\n')
    app_file.write('app.add_route(' + app_name + '_app_urls[\'' + app_name + '_collection\'][0],' + app_name + '_app_urls[\'' + app_name + '_collection\'][1])\n')
    app_file.write('app.add_route(' + app_name + '_app_urls[\'' + app_name + '_single\'][0],' + app_name + '_app_urls[\'' + app_name + '_single\'][1])\n\n')
    app_file.close()


def gen_app(apps_list):
    current_path = os.getcwd()

    for app in apps_list:
        path= current_path + '/apps/'+ app+'s'
        os.makedirs(path)
        gen_models(path, app)
        gen_resources(path, app)
        gen_urls(path,app)
        edit_app(current_path,app)


print("Enter one or more apps names separated by space. ")
list_app = list(map(str,input("\nEnter the app_names : ").strip().split())) 
  
print(list_app)
gen_app(list_app)