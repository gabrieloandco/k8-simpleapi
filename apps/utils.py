import pprint
from sqlalchemy import *
from sqlalchemy.orm import relationship, class_mapper, scoped_session, sessionmaker
from settings.environment import DB_URI
from random import random, seed
import string

pp = pprint.PrettyPrinter(indent=4)

engine = create_engine(DB_URI)
metadata = MetaData(bind=engine)

factory = sessionmaker(bind=engine)
session = scoped_session(factory)

conn = engine.connect()

class Base(object):
    # Name of the table in DB
    __tablename__ = ''

    # Table to be used in the class
    __table__ = None

    # Id from which 
    id_lookup = 'id'

    # Fields that reference other tables
    foreigns = []

    # Fields that reference other tables and the reference is in another table
    ext_foreigns = []

    # Fields that reference many to many relations
    many_to_many = []

    @classmethod
    def _get_keys(cls):
        return class_mapper(cls).c.keys()

    def get_dict(self):
        d = {}

        # Packs model default fields
        for key in self._get_keys():
            d[key] = str(getattr(self, key))

    def get_tablename(self):
        return self.__tablename__

    def get_id_lookup(self):
        return self.id_lookup


def get_table_rows(table):
    keys = table.columns.keys()
    select_st = select([table])
    res = conn.execute(select_st)
    tmp = []
    for _row in res:
        tmp.append(_row)
    table_rows=[]
    for row in tmp:
        values = row.values()
        tmp_row = {}
        for key, value in zip(keys, values):
            tmp_row[key] = value
        table_rows.append(tmp_row)

    return table_rows

