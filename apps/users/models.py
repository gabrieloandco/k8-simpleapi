from settings.environment import DB_URI
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
APP_NAME = 'user'

Base = declarative_base()
engine = create_engine(DB_URI)
metadata = MetaData(bind=engine)

# Write below your models, heres a functional example:

class User(Base):
	__tablename__ = 'users'
	id = Column(Integer, primary_key=True)
	email = Column(String(50))
	password = Column(String(200))

