from settings.environment import DB_URI
from sqlalchemy import create_engine
from .resources import *

db_engine = create_engine(DB_URI)

# Write below your urls with his resources into a tuple.
user_app_urls = {

'user_collection' : ('/users', UserCollectionResource(db_engine)),
'user_single' : ('/users/{id}', UserSingleResource(db_engine)),
'user_signin' : ('/users/signin/', UserSigninResource(db_engine)),

}