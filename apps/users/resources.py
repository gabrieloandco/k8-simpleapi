import falcon, json
import datetime
import pprint
from falcon_autocrud.resource import CollectionResource, SingleResource
from .models import *
from settings.environment import DB_URI
from sqlalchemy import *
from sqlalchemy.orm import scoped_session, sessionmaker
from passlib.hash import pbkdf2_sha256

engine = create_engine(DB_URI)
factory = sessionmaker(bind=engine)
session = scoped_session(factory)

pp = pprint.PrettyPrinter(indent=4)

# Write below the resources of your endpoint, heres a functional CRUD example:
class UserCollectionResource(CollectionResource):
# this are his defaults methods, limit them as you wish.
	methods = ['GET', 'POST']
	model = User


	def on_post(self, req, resp):
		print(req.context)
		data = req.context['doc']
		encrypted_password = pbkdf2_sha256.using(rounds=100000, salt_size=10).hash(data['password'])
		user = User(email=data['email'], password=encrypted_password)
		session.add(user)
		session.commit()
		resp.media = data
		resp.status = falcon.HTTP_200

class UserSingleResource(SingleResource):
# this are his defaults methods, limit them as you wish.
	methods = ['GET','PUT','PATCH','DELETE']
	model = User


class UserSigninResource(SingleResource):
# this are his defaults methods, limit them as you wish.
	methods = ['POST']
	model = User

	def on_post(self, req, resp):
		data = req.context['doc']
		try:
			user = session.query(User).filter_by(email=data['email']).first()
		except:
			resp.status = falcon.HTTP_404
			return
		check_password = pbkdf2_sha256.verify(data['password'], user.password)
		if not check_password:
			resp.status = falcon.HTTP_401
			return
		resp.media = data
		resp.status = falcon.HTTP_200

