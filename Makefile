local_build:
	minikube start
	eval $(minikube docker-env)
	docker build -t falcon-kubernetes .

local_deploy:
	minikube start
	kubectl apply -f ./kubernetes

local_test:
	pytest test.py

local_destroy:
	kubectl delete all --all
	kubectl delete pvc --all
	kubectl delete pv --all
	kubectl delete secrets --all

create_cluster:
	eksctl create cluster --name=new-cluster --nodes=3 --node-type t3.micro
	aws eks update-kubeconfig --name new-cluster

build:
	sudo docker login
	sudo docker build -t gabrieloandco/falcon-kubernetes .
	sudo docker push gabrieloandco/falcon-kubernetes
	sudo kubectl create secret generic regcred  --from-file=.dockerconfigjson=/home/bonny/.docker/config.json --type=kubernetes.io/dockerconfigjson || true

deploy:
	#helm repo add ingress-nginx   https://kubernetes.github.io/ingress-nginx
	#helm install   my-nginx  ingress-nginx/ingress-nginx
	kubectl apply -f ./kubernetes

test:
	pytest test.py


destroy_cluster:
	eksctl delete cluster --name=new-cluster