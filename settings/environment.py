import os


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

#DB_URI = 'postgresql+psycopg2://postgres:postgres@localhost/falcon_db'

DB_URI = 'postgresql+psycopg2://%s:%s@%s/%s' % (os.environ.get('DB_USER'),os.environ.get('DB_PASS'), os.environ.get('DB_HOST'),os.environ.get('DB_NAME'))


