# K8 deployed REST API
This is a simple REST API written in python using the Falcon Framework https://falconframework.org/ and SQLAlchemy as the ORM: https://www.sqlalchemy.org/

The Database Engine used is PostgreSQL: https://www.postgresql.org/

To run the application locally without Kubernetes you have to install a PostgreSQL server, create a database and also set the environment variables 'DB_USER', 'DB_PASS', 'DB_HOST' and 'DB_NAME'. 

Then you should install the libraries in your environment with `pip install -r requirements.txt` and you can run the app with `gunicorn app`

## API Endpoints
GET /users/{id} - Retrieves a specific user

POST /users - Creates a new user. Inputs: {email,password}

GET /users/ - Retrieves all users

DELETE /users/{id} - Deletes a specific user

POST /users/signin/ - Verifies the information of given user Inputs: {email,password}
## Working locally
Checkout to the develop branch: `git checkout develop`

First install minikube: https://kubernetes.io/docs/tasks/tools/install-minikube/ and kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/

### Building the docker image and registering it in minikube registry
Run `make local_build`

### Testing locally
Run `make local_test`

### Creating the cluster locally 
Run `make local_deploy`

## Working in AWS
Checkout the production branch `git checkout master`
### Creating the cluster in AWS EKS
Install the AWS cli: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

Create a IAM user and copy the secret and access keys. 

Run `aws configure` and insert your credentials and region. 

Install kubectl:

Install eksctl: https://eksctl.io/introduction/#installation


Run `make create_cluster` this will create a cluster in AWS EKS and apply the configuration in kubectl.If you want to check if it worked run `kubectl get nodes`.


### Building and uploading to dockerhub
Create an account and repo in DockerHub, edit the Makefile with your user and repo and run `make build`

### First Deploy
Run `make deploy`

### Automated Tests
They are ran in GitlabCI. 

### Test the API yourself
If you are using the remote server first run `kubectl get pods` and `kubectl port-forward [POD-NAME] 8000:8000` and then in another terminal you can try the endpoints with httpie (`pip install httpie`) example: `http 127.0.0.1:8000/users`

### Integrating with Gitlab
Run the first deploy with run `make deploy`

List all secrets with `kubectl get secrets`. There will be a secret starting with gitlab-service-account-token-. Copy the NAME for this secret, and run the following command to see the token: `kubectl describe secret [NAME]`. Copy the token that is part of the output, and enter it in GitLab as USER_TOKEN.

Get the certificateAuthority and the endpoint with `aws eks describe-cluster --name new-cluster`


Configure the following variables, CERTIFICATE_AUTHORITY_DATA, DOCKER_USER, DOCKER_PASSWORD, SERVER (the endpoint from the last step), USER_TOKEN.

### Logging 
For the logging I would use EKS control panel logging: https://docs.aws.amazon.com/eks/latest/userguide/control-plane-logs.html and a Sentry server for error tracking: https://sentry.io/

## Recommendations 
For having a production environment reachable from the outside world we need a domain to be set up on Route53 and also to set up an ALB ingress: https://docs.aws.amazon.com/eks/latest/userguide/alb-ingress.html

If you plan to create a cloud architecture including the EKS cluster you could use terraform:
https://learn.hashicorp.com/tutorials/terraform/eks
I would actually recommend to use an RDS database instead of a Kubernetes pod for production.

If you have the resources you could use EKS integration of Gitlab but I found it quite buggy:
https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/
https://about.gitlab.com/blog/2020/05/05/deploying-application-eks/


